--[[
LegionTD
by: Critwhale and Kobb
Dance Party: (ノ￣ー￣)ノ ヘ(￣ω￣ヘ) ~(‾▿‾~) ◝(・ω・)◟

This file to only handle stuff related to game states.
]]--

---------------------------------------------------------------------------
-- LegionTD Gamemode Class
---------------------------------------------------------------------------

-- add Gamemode to global Scope
if LegionTDGamemode == nil then
    _G.LegionTDGameMode = class({})
end

--[[
-- ThinkStates
_thinkState_PreGame
    This is before the game actually begins.
_thinkState_PreRound
    The Build Area of a round.
_thinkState_InRound
    The Wave Part of the round.
_thinkState_ArenaRound
    The Wave Part of the round.
_thinkState_PostGame
    After a King has fallen.
]]--

-- Constants
GAMETIME_PRE = 10
GAMETIME_POST = 30

ROUND_BUILDTIME = 10

require('Rounds')
require('critwhale_building_helper')

function LegionTDGameMode:InitGameMode()
    print( "LegionTD Initialized." )
    self = _G.LegionTDGameMode

    -- Setup Variables
    self.nLooptime = 0.1
    self.nRound = 1
    self.sCurrentState = "_thinkState_PreGame"
    self.tPlayerList = {}
    self.tRadiantTeam = {}
    self.tDireTeam = {}
    self.tWaveUnits = { ['Radiant'] = { [1]={}, [2]={}, [3]={}, [4]={} }, ['Dire'] = { [1]={}, [2]={}, [3]={}, [4]={} } }
    self.tTeleporterBool = { ['Radiant'] = {}, ['Dire'] = {} }

    -- Setup Map Entities
    LegionTDGameMode:GetMapEntities()

    -- Setup GameRules
    GameRules:SetPreGameTime( GAMETIME_PRE )

    -- Setup Initial ThinkState
    LegionTDGameMode.thinkState = Dynamic_Wrap( LegionTDGameMode, '_thinkState_PreGame' )

    local timer_info =
    {
        timerActive = true,
        timerString = "Pre-game",
        timerStartTime = GameRules:GetGameTime(),
        timerDuration = 10,
        panelWidth = 250
    }
    CustomGameEventManager:Send_ServerToAllClients("think_state_change", timer_info)

    -- Setup Game Event Listeners
    -- ListenToGameEvent( "game_rules_state_change", Dynamic_Wrap( LegionTDGameMode, 'OnGameRulesStateChange' ), LegionTDGameMode )
    --ListenToGameEvent('entity_hurt', Dynamic_Wrap( LegionTDGameMode, 'OnDamageTaken'), self)
    --ListenToGameEvent('npc_spawned', Dynamic_Wrap( LegionTDGameMode, 'OnNPCSpawned'), self)
    CustomGameEventManager:RegisterListener( "legiontd_unit_remove", OnUnitRemove )
    CustomGameEventManager:RegisterListener( "legiontd_unit_upgrade", OnUnitUpgrade )

    -- Setup Commands

    -- Populate Entitie Variables
    LegionTDGameMode:GetMapEntities()

    -- Setup Thinker
    GameRules:GetGameModeEntity():SetThink( "OnThink", LegionTDGameMode, self.nLooptime )
end

function LegionTDGameMode:OnThink()

    -- Pauses would fuck the timer otherwise.
    if GameRules:IsGamePaused() == true then
        return self.nLooptime
    end

    -- Set the state of heros so they can never unphase/getattacked.
    for k, hero in pairs(HeroList:GetAllHeroes()) do
        hero:AddNewModifier(hero,nil,'modifier_invulnerable', {duration = 2} )
        hero:AddNewModifier(hero,nil,'modifier_phased', {duration = 2} )
    end

    LegionTDGameMode:CalculateGameTime()
    LegionTDGameMode:UpdatePlayerStatus()
    LegionTDGameMode.thinkState()

    return self.nLooptime
end

---------------------------------------------------------------------------
-- thinkState PreGame
---------------------------------------------------------------------------
function LegionTDGameMode:_thinkState_PreGame()

    -- Check if game is still in pre game, or earlier state, if so do nothing.
    if GameRules:State_Get() <= DOTA_GAMERULES_STATE_PRE_GAME then
        return
    end

    LegionTDGameMode:ChangeThinkState( '_thinkState_PreRound' )
end

---------------------------------------------------------------------------
-- thinkState PreRound
---------------------------------------------------------------------------
function LegionTDGameMode:_thinkState_PreRound()

    --Initialize PrepTime If Needed.
    if LegionTDGameMode.PrepTimeLeft == nil then
            LegionTDGameMode.PrepTimeLeft = ROUND_BUILDTIME

            for k, hero in pairs(HeroList:GetAllHeroes()) do
                for i=0,6 do
                    local ability = hero:GetAbilityByIndex(i)
                    if ability then
                        ability:SetLevel(1)
                    end
                end
            end
    end

    -- Calculate PrepTimeLeft
    LegionTDGameMode.PrepTimeLeft = math.max (0, LegionTDGameMode.PrepTimeLeft - LegionTDGameMode.dt)

    -- Check if PrepTimeLeft is 0 and move to wave time if needed.
    if LegionTDGameMode.PrepTimeLeft <= 0 then
        LegionTDGameMode:ChangeThinkState( '_thinkState_InRound' )
        LegionTDGameMode.PrepTimeLeft = nil
    end

end

---------------------------------------------------------------------------
-- thinkState InRound
---------------------------------------------------------------------------
function LegionTDGameMode:_thinkState_InRound()

    -- Attempt to spawn a single wave unit in every Active Lane.
    LegionTDGameMode:SpawnWaveUnits()

    BuildingHelper:Attack()

    if LegionTDGameMode:CheckWaveCleared() == true then
        print('Wave ' .. LegionTDGameMode.nRound .. ' Cleared')
        BuildingHelper:RespawnBuildings()
        LegionTDGameMode.nRound = LegionTDGameMode.nRound + 1
        LegionTDGameMode.UnitsLeftToSpawn = nil
        LegionTDGameMode:ChangeThinkState( '_thinkState_PreRound' )
    end
end

---------------------------------------------------------------------------
-- thinkState InRound
---------------------------------------------------------------------------
function LegionTDGameMode:_thinkState_PostGame()
end

---------------------------------------------------------------------------
-- thinkState Change
---------------------------------------------------------------------------
function LegionTDGameMode:ChangeThinkState( sThinkState )
    local thinkState_Info = {}

    if sThinkState == '_thinkState_InRound' then
        thinkState_Info =
        {
            canBuild = false,
            timerActive = false,
            timerString = "In-Round",
            timerStartTime = LegionTDGameMode.currentTimerInit,
            timerDuration = 15,
            panelWidth = 250,
            usingTimer = false
        }

    else
        thinkState_Info =
        {
            canBuild = true,
            timerActive = true,
            timerString = "Build Time Left",
            timerStartTime = LegionTDGameMode.currentTimerInit,
            timerDuration = 15,
            panelWidth = 250,
            usingTimer = true
        }
    end

    print('[[LegionTD]] GameState Entering ' .. sThinkState)
    CustomGameEventManager:Send_ServerToAllClients("thinkStateChanged", thinkState_Info)
    LegionTDGameMode.sCurrentState = "sThinkState"
    LegionTDGameMode.thinkState = Dynamic_Wrap( LegionTDGameMode, sThinkState )
end

---------------------------------------------------------------------------
-- Calculate Game Time
---------------------------------------------------------------------------
function LegionTDGameMode:CalculateGameTime()
    -- Track game time, since the dt passed in to think is actually wall-clock time not simulation time.
    local now = GameRules:GetGameTime()
    if LegionTDGameMode.t0 == nil then
        LegionTDGameMode.t0 = now
    end
    LegionTDGameMode.dt = now - LegionTDGameMode.t0
    LegionTDGameMode.t0 = now
end

---------------------------------------------------------------------------
-- thinkState Change
---------------------------------------------------------------------------
function LegionTDGameMode:CheckWaveCleared()
    local nTotalCount = 0
    local nTotalAlive = 0

    for A,B in ipairs(LegionTDGameMode.tWaveUnits.Radiant) do
        local nCount = 0
        local nAlive = 0

        for k,v in pairs(B) do
            nTotalCount = nTotalCount + 1
            nCount = nCount + 1
            if not v:IsNull() then
                if v:IsAlive() then
                    nTotalAlive = nTotalAlive + 1
                    nAlive = nAlive + 1
                end
            end
        end

        if nAlive == 0 then
            LegionTDGameMode.tTeleporterBool.Radiant[A] = true
        else
            LegionTDGameMode.tTeleporterBool.Radiant[A] = false
        end
    end

    for A,B in ipairs(LegionTDGameMode.tWaveUnits.Dire) do
        local nCount = 0
        local nAlive = 0

        for k,v in pairs(B) do
            nTotalCount = nTotalCount + 1
            nCount = nCount + 1
            if not v:IsNull() then
                if v:IsAlive() then
                    nTotalAlive = nTotalAlive + 1
                    nAlive = nAlive + 1
                end
            end
        end

        if nAlive == 0 then
            LegionTDGameMode.tTeleporterBool.Dire[A] = true
        else
            LegionTDGameMode.tTeleporterBool.Dire[A] = false
        end
    end

    if nTotalAlive == 0 then
        return true
    end
end

---------------------------------------------------------------------------
-- Update Player Status
--------------------------------------------------------------------------
function LegionTDGameMode:UpdatePlayerStatus()

    --Reset the tables
    LegionTDGameMode.tPlayerList = {}
    LegionTDGameMode.tRadiantTeam = {}
    LegionTDGameMode.tDireTeam = {}

    for playerID = 0, (DOTA_MAX_TEAM_PLAYERS-1) do
        local player = PlayerResource:GetPlayer(playerID)

        if PlayerResource:IsValidTeamPlayer(playerID) then

            if PlayerResource:GetConnectionState(playerID) == 2 then
                if PlayerResource:GetTeam(playerID) == 2 then
                    table.insert(LegionTDGameMode.tRadiantTeam, true)
                elseif PlayerResource:GetTeam(playerID) == 3 then
                    table.insert(LegionTDGameMode.tDireTeam, true)
                end
            else
                if PlayerResource:GetTeam(playerID) == 2 then
                    table.insert(LegionTDGameMode.tRadiantTeam, false)
                elseif PlayerResource:GetTeam(playerID) == 3 then
                    table.insert(LegionTDGameMode.tDireTeam, false)
                end
            end

            local nRadiant = 0
            for k,v in ipairs(LegionTDGameMode.tRadiantTeam) do
                nRadiant = nRadiant + 1
            end
            local nDire = 0
            for k,v in ipairs(LegionTDGameMode.tDireTeam) do
                nDire = nDire + 1
            end

            if PlayerResource:GetTeam(playerID) == 2 then
                LegionTDGameMode.tPlayerList[playerID] = nRadiant
            elseif PlayerResource:GetTeam(playerID) == 3 then
                LegionTDGameMode.tPlayerList[playerID] = nDire
            end
        end
    end
end

---------------------------------------------------------------------------
-- Spawn Wave Units
---------------------------------------------------------------------------
function LegionTDGameMode:SpawnWaveUnits()
    local round = LegionTDGameMode.nRound
    local unitList = RoundList.Round[round]

    --Initialize UnitsLeftToSpawn If Needed.
    if LegionTDGameMode.UnitsLeftToSpawn == nil then

        local count = 0
        if unitList ~= nil then
            for k,v in ipairs(unitList) do
                count = count + 1
            end
        end

        LegionTDGameMode.UnitsLeftToSpawn = count
    end

    if LegionTDGameMode.UnitsLeftToSpawn > 0 then
        LegionTDGameMode:SpawnSingleWaveUnit()
        LegionTDGameMode.UnitsLeftToSpawn = LegionTDGameMode.UnitsLeftToSpawn - 1
    end
end

---------------------------------------------------------------------------
-- Spawna single wave Units
---------------------------------------------------------------------------

function LegionTDGameMode:SpawnSingleWaveUnit()
    local round = LegionTDGameMode.nRound
    local unitList = RoundList.Round[round]

    for k, v in ipairs(LegionTDGameMode.tRadiantTeam) do
        if v == true then
            local sUnitName = unitList[LegionTDGameMode.UnitsLeftToSpawn]
            local MapEntities = LegionTDGameMode.MapEntities
            local path = {
                [1] = {MapEntities.WaveSpawn.R_TL, MapEntities.WavePath.R_L, MapEntities.WavePath.R_C1, LegionTDGameMode.MapEntities.KingSpawnR },
                [2] = {MapEntities.WaveSpawn.R_BL, MapEntities.WavePath.R_L, MapEntities.WavePath.R_C1, LegionTDGameMode.MapEntities.KingSpawnR },
                [3] = {MapEntities.WaveSpawn.R_TR, MapEntities.WavePath.R_R, MapEntities.WavePath.R_C1, LegionTDGameMode.MapEntities.KingSpawnR },
                [4] = {MapEntities.WaveSpawn.R_BR, MapEntities.WavePath.R_R, MapEntities.WavePath.R_C1, LegionTDGameMode.MapEntities.KingSpawnR },
            }

            -- Create The unit
            local WaveUnit = CreateUnitByName(sUnitName, path[k][1]:GetOrigin(), true, nil, nil, DOTA_TEAM_BADGUYS)

            -- Give the A click command to the unit so It can walk down lane correctly.
            local Order1 = { UnitIndex = WaveUnit:entindex(), OrderType = DOTA_UNIT_ORDER_ATTACK_MOVE, TargetIndex = nil, AbilityIndex = 0, Position = path[k][2]:GetOrigin(), Queue = true }
            local Order2 = { UnitIndex = WaveUnit:entindex(), OrderType = DOTA_UNIT_ORDER_ATTACK_MOVE, TargetIndex = nil, AbilityIndex = 0, Position = path[k][3]:GetOrigin(), Queue = true }
            local Order3 = { UnitIndex = WaveUnit:entindex(), OrderType = DOTA_UNIT_ORDER_ATTACK_MOVE, TargetIndex = nil, AbilityIndex = 0, Position = path[k][4]:GetOrigin(), Queue = true }

            ExecuteOrderFromTable(Order1)
            ExecuteOrderFromTable(Order2)
            ExecuteOrderFromTable(Order3)

            table.insert(LegionTDGameMode.tWaveUnits.Radiant[k], WaveUnit)
        end
    end

    for k, v in ipairs(LegionTDGameMode.tDireTeam) do
        if v == true then
            local sUnitName = unitList[LegionTDGameMode.UnitsLeftToSpawn]
            local MapEntities = LegionTDGameMode.MapEntities
            local path = {
                [1] = {MapEntities.WaveSpawn.D_TL, MapEntities.WavePath.D_L, MapEntities.WavePath.D_C1, LegionTDGameMode.MapEntities.KingSpawnD },
                [2] = {MapEntities.WaveSpawn.D_BL, MapEntities.WavePath.D_L, MapEntities.WavePath.D_C1, LegionTDGameMode.MapEntities.KingSpawnD },
                [3] = {MapEntities.WaveSpawn.D_TR, MapEntities.WavePath.D_R, MapEntities.WavePath.D_C1, LegionTDGameMode.MapEntities.KingSpawnD },
                [4] = {MapEntities.WaveSpawn.D_BR, MapEntities.WavePath.D_R, MapEntities.WavePath.D_C1, LegionTDGameMode.MapEntities.KingSpawnD },
            }

            -- Create The unit
            local WaveUnit = CreateUnitByName(sUnitName, path[k][1]:GetOrigin(), true, nil, nil, DOTA_TEAM_GOODGUYS)

            -- Give the A click command to the unit so It can walk down lane correctly.
            local Order1 = { UnitIndex = WaveUnit:entindex(), OrderType = DOTA_UNIT_ORDER_ATTACK_MOVE, TargetIndex = nil, AbilityIndex = 0, Position = path[k][2]:GetOrigin(), Queue = true }
            local Order2 = { UnitIndex = WaveUnit:entindex(), OrderType = DOTA_UNIT_ORDER_ATTACK_MOVE, TargetIndex = nil, AbilityIndex = 0, Position = path[k][3]:GetOrigin(), Queue = true }
            local Order3 = { UnitIndex = WaveUnit:entindex(), OrderType = DOTA_UNIT_ORDER_ATTACK_MOVE, TargetIndex = nil, AbilityIndex = 0, Position = path[k][4]:GetOrigin(), Queue = true }

            ExecuteOrderFromTable(Order1)
            ExecuteOrderFromTable(Order2)
            ExecuteOrderFromTable(Order3)

            table.insert(LegionTDGameMode.tWaveUnits.Dire[k], WaveUnit)
        end
    end
end

---------------------------------------------------------------------------
-- Load Map Entities
---------------------------------------------------------------------------
function LegionTDGameMode:GetMapEntities()
    local MapEntities = {["WaveSpawn"] = {}, ["WavePath"] = {}}

    -- King Spawn Locations
    MapEntities.KingSpawnR = Entities:FindByName(nil, 'KingSpawn_R')
    MapEntities.KingSpawnD = Entities:FindByName(nil, 'KingSpawn_D')

    -- Wave creep spawn locations.
    MapEntities.WaveSpawn.R_TL = Entities:FindByName(nil, 'WaveSpawn_R_TL')
    MapEntities.WaveSpawn.R_TR = Entities:FindByName(nil, 'WaveSpawn_R_TR')
    MapEntities.WaveSpawn.R_BL = Entities:FindByName(nil, 'WaveSpawn_R_BL')
    MapEntities.WaveSpawn.R_BR = Entities:FindByName(nil, 'WaveSpawn_R_BR')

    MapEntities.WaveSpawn.D_TL = Entities:FindByName(nil, 'WaveSpawn_D_TL')
    MapEntities.WaveSpawn.D_TR = Entities:FindByName(nil, 'WaveSpawn_D_TR')
    MapEntities.WaveSpawn.D_BL = Entities:FindByName(nil, 'WaveSpawn_D_BL')
    MapEntities.WaveSpawn.D_BR = Entities:FindByName(nil, 'WaveSpawn_D_BR')

    -- The Path the wave creeps take.
    MapEntities.WavePath.R_L = Entities:FindByName(nil, 'WavePath_R_L')
    MapEntities.WavePath.R_R = Entities:FindByName(nil, 'WavePath_R_R')
    MapEntities.WavePath.R_C1 = Entities:FindByName(nil, 'WavePath_R_C_1')

    MapEntities.WavePath.D_L = Entities:FindByName(nil, 'WavePath_D_L')
    MapEntities.WavePath.D_R = Entities:FindByName(nil, 'WavePath_D_R')
    MapEntities.WavePath.D_C1 = Entities:FindByName(nil, 'WavePath_D_C_1')

    -- Teleports
    MapEntities.WaveTeleR = Entities:FindByName(nil, 'FriendlyTP_R')
    MapEntities.WaveTeleD = Entities:FindByName(nil, 'FriendlyTP_D')

    local RadiantKing = CreateUnitByName("npc_dota_creature_BuildingTest_King", MapEntities.KingSpawnR:GetOrigin(), true, nil, nil, DOTA_TEAM_GOODGUYS)

    LegionTDGameMode.MapEntities = MapEntities
end
