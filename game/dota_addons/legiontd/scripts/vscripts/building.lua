-- TODO Add checks for if can build here.
-- TODO Add Check for unit Rotation.
-- TODO Implement a system to place the correct Building.
-- TODO Add Check to see if unit is already in the place of the tower being made.
-- TODO get Kobb to add a visual indicator for ability failed cast.
-- TODO Polish, Add particle effect during cast start.

triggers = {'trigger_TL'}
growingBuildings = {}

-- Called from a building KV to create a building at location. Should work for all buildings.
function BuildBuilding( keys )
    local caster = keys.caster
    local target = keys.target_points[1]
    local AbilityName = keys.AbilityName
    local buildingName = keys.Building
    local triggerInfo
    local player = caster:GetPlayerOwner()

    -- Snap the unit to closest grid Location.
    target = Vector( RoundUp(target.x), RoundUp(target.y), RoundUp(target.z))

    -- Callback for when the building is created, Starts the growth of the building, and rotates it based on the rotation of the trigger.
    local createUnitCallback = function(unit)
        unit:SetHealth( 1 )
        unit:SetForwardVector(triggerInfo.triggerFV)

        local Building = {
            Unit = unit,
            BuildingName = buildingName,
            Owner = keys.caster,
            Team = caster:GetTeam(),
            ModelScale = unit:GetModelScale(),
            GrowthRate = 5,
            Position = target,
            TriggerInfo = triggerInfo,
            ForwardVector = triggerFV
        }
        unit:SetModelScale((unit:GetModelScale() / 2))

        _G.BuildingHelper:CreateBuilding(Building)
    end

    triggerInfo = _G.BuildingHelper:checkForTriggers(Entities:FindAllInSphere(GetGroundPosition(target, nil), 10))

    -- Checks if the ability was in a play area trigger, and if so Casts the tower, Otherwise resets the cooldown.
    if triggerInfo.inTrigger then
        if checkForBuildingInPosition( target ) == false then
            Building = CreateUnitByNameAsync(buildingName, target, false, caster, caster, caster:GetTeam(), createUnitCallback)

        else
            local ability = caster:FindAbilityByName(AbilityName)
            ability:EndCooldown()
            local error_info =
            {
                errorType = "legiontd_error_invalid_location",
            }
            CustomGameEventManager:Send_ServerToPlayer(player, "error_message_show", error_info)
        end
    else
        local ability = caster:FindAbilityByName(AbilityName)
        ability:EndCooldown()
        local error_info =
        {
            errorType = "legiontd_error_invalid_location",
        }
        CustomGameEventManager:Send_ServerToPlayer(player, "error_message_show", error_info)
    end

end


-- For Rounding Up to a multiple of the building Grid.
function RoundUp( toRound )
    return (128 - toRound % 128) + toRound - 64
end
