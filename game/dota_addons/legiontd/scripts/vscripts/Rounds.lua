--[[
LegionTD
    by: Critwhale and Kobb
    Dance Party: (ノ￣ー￣)ノ ヘ(￣ω￣ヘ) ~(‾▿‾~) ◝(・ω・)◟
 ]]--

 -- add Gamemode to global Scope
 if RoundList == nil then
     _G.RoundList = class({})
 end
RoundList.Round = {}

RoundList.Round[1] = {
        -- Units Go here via string
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",

        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",

        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",

        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",

        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",

        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",

        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",
        "npc_legiontd_creature_wave_1",

        "npc_legiontd_creature_wave_1"
    }

RoundList.Round[2] = {
    -- Units Go here via string
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",

    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",

    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",

    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",

    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",

    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",

    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",

    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",

    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2",
    "npc_legiontd_creature_wave_2"

    }

RoundList.Round[3] = {
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",

    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",

    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",

    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",

    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",

    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",

    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",

    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3",
    "npc_legiontd_creature_wave_3"
}
