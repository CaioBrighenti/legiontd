require('legiontd')

---------------------------------------------------------------------------
-- Precache
---------------------------------------------------------------------------

function Precache( context )
	--[[
		Precache things we know we'll use.  Possible file types include (but not limited to):
			PrecacheResource( "model", "*.vmdl", context )
			PrecacheResource( "soundfile", "*.vsndevts", context )
			PrecacheResource( "particle", "*.vpcf", context )
			PrecacheResource( "particle_folder", "particles/folder", context )
	]]

		PrecacheResource("model", "models/items/furion/treant/shroomling_treant/shroomling_treant.vmdl", context)
		PrecacheResource("model", "models/courier/defense3_sheep/defense3_sheep.vmdl", context)

		--Radiant King Stuff
		PrecacheResource("model", "models/heroes/kunkka/kunkka.vmdl", context)
		PrecacheResource("model", "models/items/kunkka/style_of_the_divine_anchor/style_of_the_divine_anchor.vmdl", context)
		PrecacheResource("model", "models/items/kunkka/boots_of_the_divine_anchor/boots_of_the_divine_anchor.vmdl", context)
		PrecacheResource("model", "models/items/kunkka/collar_of_the_divine_anchor/collar_of_the_divine_anchor.vmdl", context)
		PrecacheResource("model", "models/items/kunkka/cuffs_of_the_divine_anchor/cuffs_of_the_divine_anchor.vmdl", context)
		PrecacheResource("model", "models/items/kunkka/epaulets_of_the_divine_anchor/epaulets_of_the_divine_anchor.vmdl", context)
		PrecacheResource("model", "models/items/kunkka/flintlock_of_the_divine_anchor/flintlock_of_the_divine_anchor.vmdl", context)
		PrecacheResource("model", "models/items/kunkka/medallion_of_the_divine_anchor/medallion_of_the_divine_anchor.vmdl", context)
		PrecacheResource("model", "models/items/kunkka/cyclone_wave_smasher_of_the_divine_anchor/cyclone_wave_smasher_of_the_divine_anchor.vmdl", context)

		PrecacheResource( "model", "models/items/necrolyte/death_protest_weapon/death_protest_weapon.vmdl", context )
		PrecacheResource("model", "models/heroes/chaos_knight/chaos_knight_shoulderpads.vmdl", context)
		PrecacheResource("model", "models/heroes/chaos_knight/chaos_knight_helmet.vmdl", context)
		PrecacheResource("model", "models/heroes/chaos_knight/chaos_knight_mace.vmdl", context)
		PrecacheResource("model", "models/heroes/chaos_knight/chaos_knight_shield.vmdl", context)
		PrecacheResource("model", "models/heroes/chaos_knight/chaos_knight.vmdl", context)

		--Tower Models

		--Creep Models
		PrecacheResource("model", "models/courier/defense3_sheep/defense3_sheep.vmdl", context)
		PrecacheResource("model", "models/items/courier/amphibian_kid/amphibian_kid.vmdl", context)
		PrecacheResource("model", "models/items/courier/nexon_turtle_01_grey/nexon_turtle_01_grey.vmdl", context)

end

-- Create the game mode and Initialize it.
function Activate()
	GameRules.LegionTD = LegionTDGameMode()
	GameRules.LegionTD:InitGameMode()
end
