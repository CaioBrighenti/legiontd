-- Only to be used with Lefion TD

if BuildingHelper == nil then
    print('[BuildingHelper] loaded')
    _G.BuildingHelper = {}
    BuildingHelper = _G.BuildingHelper
    BuildingHelper.__index = BuildingHelper
end

Buildings = {}

-- Creates the building Tables
function Buildings:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function BuildingHelper:Initialize()
    BuildingHelper = self
    self.Buildings = {}

    if self.thinkEnt == nil then

        self.thinkEnt = Entities:CreateByClassname("info_target")
        self.thinkEnt:SetThink("Think", self, "buildingHelper", BuildingHelper)
    end
end

function BuildingHelper:Think()

    if GameRules:State_Get() < DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
        return 0.1
    elseif GameRules:State_Get() >= DOTA_GAMERULES_STATE_POST_GAME then
        return
    end

    for k, v in pairs(self.Buildings) do

        -- Whatever Else I need to add on a timer
    end
    self.growBuildings()

    return 0.1
end

function BuildingHelper:CreateBuilding(uBuilding)

    if uBuilding.Unit == nil or uBuilding.Position == nil or uBuilding.ForwardVector then
        return nil
    end

    -- Defaults just in case they weren't set.
    uBuilding.ModelScale = uBuilding.ModelScale or 1
    uBuilding.GrowthRate = uBuilding.GrowthRate or 5
    uBuilding.Tele = false
    uBuilding.TeleAttack = false
    uBuilding.Attack = false
    uBuilding.FinishedGrowth = false

    function uBuilding.Unit:GetBuildingAttachment()
        for k, v in pairs(self.Buildings) do
            if k.Unit == uBuilding.Unit then
                return k
            end
        end
    end

    self.Buildings[uBuilding] = uBuilding

    return self.Buildings[uBuilding]
end

function BuildingHelper:RemoveBuilding(uBuilding)
    BuildingHelper.Buildings[uBuilding] = nil
end

function BuildingHelper:checkForTriggers(table)
    local triggerInfo = {}
    triggerInfo.inTrigger = false

    -- I did this as lazily as possible.
    for _,thing in pairs(table) do

        if (thing:GetName() == "trigger_R_TL") then
            triggerInfo.inTrigger = true
            triggerInfo.triggerName = 'trigger_R_TL'
            triggerInfo.triggerFV = Vector(0,1,0)
            break

        elseif (thing:GetName() == "trigger_R_TR") then
            triggerInfo.inTrigger = true
            triggerInfo.triggerName = 'trigger_R_TR'
            triggerInfo.triggerFV = Vector(0,1,0)
            break

        elseif (thing:GetName() == "trigger_R_BL") then
            triggerInfo.inTrigger = true
            triggerInfo.triggerName = 'trigger_R_BL'
            triggerInfo.triggerFV = Vector(0,-1,0)
            break

        elseif (thing:GetName() == "trigger_R_BR") then
            triggerInfo.inTrigger = true
            triggerInfo.triggerName = 'trigger_R_BR'
            triggerInfo.triggerFV = Vector(0,-1,0)
            break

        elseif (thing:GetName() == "trigger_D_TL") then
            triggerInfo.inTrigger = true
            triggerInfo.triggerName = 'trigger_R_TL'
            triggerInfo.triggerFV = Vector(0,1,0)
            break

        elseif (thing:GetName() == "trigger_D_TR") then
            triggerInfo.inTrigger = true
            triggerInfo.triggerName = 'trigger_R_TR'
            triggerInfo.triggerFV = Vector(0,1,0)
            break

        elseif (thing:GetName() == "trigger_D_BL") then
            triggerInfo.inTrigger = true
            triggerInfo.triggerName = 'trigger_R_BL'
            triggerInfo.triggerFV = Vector(0,-1,0)
            break

        elseif (thing:GetName() == "trigger_D_BR") then
            triggerInfo.inTrigger = true
            triggerInfo.triggerName = 'trigger_R_BR'
            triggerInfo.triggerFV = Vector(0,-1,0)
            break

        else
            triggerInfo.inTrigger = false
        end
    end

    return triggerInfo
end

function BuildingHelper:CheckForTeleport(uBuilding)
    local table = Entities:FindAllInSphere(GetGroundPosition(uBuilding.Unit:GetOrigin(), nil), 10)
    local triggerInfo = {}
    triggerInfo.inTrigger = false

    if uBuilding.Tele == false then
        -- I did this as lazily as possible.
        for _,thing in pairs(table) do
            if thing:GetName() == "triggerTP_R_TL" then
                if LegionTDGameMode.tTeleporterBool.Radiant[1] == true then
                    --uBuilding.Unit:SetOrigin(LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin())
                    FindClearSpaceForUnit(uBuilding.Unit, LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin(), true)
                    uBuilding.Tele = true
                    return true
                end
            elseif thing:GetName() == "triggerTP_R_BL" then
                if LegionTDGameMode.tTeleporterBool.Radiant[2] == true then
                    --uBuilding.Unit:SetOrigin(LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin())
                    FindClearSpaceForUnit(uBuilding.Unit, LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin(), true)
                    uBuilding.Tele = true
                    return true
                end
            elseif thing:GetName() == "triggerTP_R_TR" then
                if LegionTDGameMode.tTeleporterBool.Radiant[3] == true then
                    --uBuilding.Unit:SetOrigin(LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin())
                    FindClearSpaceForUnit(uBuilding.Unit, LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin(), true)
                    uBuilding.Tele = true
                    return true
                end
            elseif thing:GetName() == "triggerTP_R_BR" then
                if LegionTDGameMode.tTeleporterBool.Radiant[4] == true then
                    --uBuilding.Unit:SetOrigin(LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin())
                    FindClearSpaceForUnit(uBuilding.Unit, LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin(), true)
                    uBuilding.Tele = true
                    return true
                end

            elseif thing:GetName() == "triggerTP_D_TL" then
                if LegionTDGameMode.tTeleporterBool.Dire[1] == true then
                    --uBuilding.Unit:SetOrigin(LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin())
                    FindClearSpaceForUnit(uBuilding.Unit, LegionTDGameMode.MapEntities.WaveTeleD:GetOrigin(), true)
                    uBuilding.Tele = true
                    return true
                end
            elseif thing:GetName() == "triggerTP_D_BL" then
                if LegionTDGameMode.tTeleporterBool.Dire[2] == true then
                    --uBuilding.Unit:SetOrigin(LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin())
                    FindClearSpaceForUnit(uBuilding.Unit, LegionTDGameMode.MapEntities.WaveTeleD:GetOrigin(), true)
                    uBuilding.Tele = true
                    return true
                end
            elseif thing:GetName() == "triggerTP_D_TR" then
                if LegionTDGameMode.tTeleporterBool.Dire[3] == true then
                    --uBuilding.Unit:SetOrigin(LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin())
                    FindClearSpaceForUnit(uBuilding.Unit, LegionTDGameMode.MapEntities.WaveTeleD:GetOrigin(), true)
                    uBuilding.Tele = true
                    return true
                end
            elseif thing:GetName() == "triggerTP_D_BR" then
                if LegionTDGameMode.tTeleporterBool.Dire[4] == true then
                    --uBuilding.Unit:SetOrigin(LegionTDGameMode.MapEntities.WaveTeleR:GetOrigin())
                    FindClearSpaceForUnit(uBuilding.Unit, LegionTDGameMode.MapEntities.WaveTeleD:GetOrigin(), true)
                    uBuilding.Tele = true
                    return true
                end
            end
        end
    end
end

function checkForBuildingInPosition( CheckPosition )
    for building, v in pairs(BuildingHelper.Buildings) do
        if building.Position == CheckPosition then

            return true
        end
    end

    return false
end

function BuildingHelper:growBuildings()
    if BuildingHelper.Buildings == nil then
        return
    end

    for uBuilding, _ in pairs( BuildingHelper.Buildings ) do

        local unit = uBuilding.Unit
        local growthRate = uBuilding.GrowthRate
        local modelScale = uBuilding.ModelScale
        local finishedGrowth = uBuilding.FinishedGrowth

        if finishedGrowth == false then

            if unit:IsNull() == false then
                if unit:IsAlive() then
                    if unit:GetHealth() < unit:GetMaxHealth() then
                        local partialPercent = (unit:GetMaxHealth() * growthRate / 100)
                        local totalPercent = (unit:GetHealth() * 100 / unit:GetMaxHealth())

                        unit:ModifyHealth(unit:GetHealth() + partialPercent, nil, false, 0)
                        unit:SetModelScale((modelScale / 2) + ((totalPercent / 100) * (modelScale/2)))
                    else
                        uBuilding.FinishedGrowth = true
                        unit:SetModelScale( modelScale )
                    end
                end
            end

        end

    end

    return 0.1
end

function BuildingHelper:RespawnBuildings()
    for uBuilding, _ in pairs( BuildingHelper.Buildings ) do
        local unit = uBuilding.Unit

        if unit:IsNull() then
            unit = CreateUnitByName(uBuilding.BuildingName, uBuilding.Position, false, uBuilding.Owner, uBuilding.Owner, uBuilding.Team)
            uBuilding.Unit = unit
        end

        uBuilding.Tele = false
        uBuilding.Attack = false
        uBuilding.TeleAttack = false
        unit:RespawnUnit()
        unit:SetOrigin(uBuilding.Position)
        Order = { UnitIndex = uBuilding.Unit:entindex(), OrderType = DOTA_UNIT_ORDER_ATTACK_MOVE, TargetIndex = nil, AbilityIndex = 0, Position = uBuilding.Position, Queue = true }
        ExecuteOrderFromTable(Order)
        unit:SetForwardVector(uBuilding.TriggerInfo.triggerFV)
    end
end

function BuildingHelper:Attack()
    local MapEntities = LegionTDGameMode.MapEntities
    local tRadiantEnts = {MapEntities.WaveSpawn.R_TL, MapEntities.WaveSpawn.R_BL, MapEntities.WaveSpawn.R_TR, MapEntities.WaveSpawn.R_BR}
    local tDireEnts = {MapEntities.WaveSpawn.D_TL, MapEntities.WaveSpawn.D_BL, MapEntities.WaveSpawn.D_TR, MapEntities.WaveSpawn.D_BR}

    for uBuilding, _ in pairs( BuildingHelper.Buildings ) do
        local Team = uBuilding.Owner:GetTeam()
        local TargetEnt = nil
        local order = nil

        if Team == 2 then
            TargetEnt = tRadiantEnts[LegionTDGameMode.tPlayerList[uBuilding.Owner:GetPlayerID()]]
        elseif Team == 3 then
            TargetEnt = tDireEnts[LegionTDGameMode.tPlayerList[uBuilding.Owner:GetPlayerID()]]
        end

        if not uBuilding.Unit:IsNull() then

            if uBuilding.Tele == true then
                local TeleEnt = false
                if Team == 2 then
                    TeleEnt = LegionTDGameMode.MapEntities.WaveTeleR
                elseif Team == 3 then
                    TeleEnt = LegionTDGameMode.MapEntities.WaveTeleD
                end

                if uBuilding.TeleAttack == false then

                    -- Give the A click command to the unit so It can walk down lane correctly.
                    Order = { UnitIndex = uBuilding.Unit:entindex(), OrderType = DOTA_UNIT_ORDER_ATTACK_MOVE, TargetIndex = nil, AbilityIndex = 0, Position = TeleEnt:GetOrigin(), Queue = false }
                    ExecuteOrderFromTable(Order)
                    uBuilding.TeleAttack = true
                end
            else
                BuildingHelper:CheckForTeleport(uBuilding)
                if uBuilding.Attack == false then

                    -- Give the A click command to the unit so It can walk down lane correctly.
                    Order = { UnitIndex = uBuilding.Unit:entindex(), OrderType = DOTA_UNIT_ORDER_ATTACK_MOVE, TargetIndex = nil, AbilityIndex = 0, Position = TargetEnt:GetOrigin(), Queue = false }
                    ExecuteOrderFromTable(Order)
                    uBuilding.Attack = true
                end
            end
        end
    end
end

function OnUnitRemove( keys )
    print('OnUnitRemove triggered')
end

function OnUnitUpgrade( keys )
    print('OnUnitUpgrade triggered')
end

BuildingHelper:Initialize()
