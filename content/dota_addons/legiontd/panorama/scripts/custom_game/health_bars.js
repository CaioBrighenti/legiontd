"use strict";

function UpdateHealthBars(data) 
{
	var team = data.health_bar_team
	var max_health = data.health_bar_max
	var current_health = data.health_bar_current
	var health_percent = current_health / max_health
	var bar_size = 250 * health_percent
	if (team == "radiant") {
		$( "#RadiantHealthBarLabel" ).text = current_health + "/" + max_health
		$( "#RadiantHealthBarImage" ).style.width = bar_size + "px";
	};
};

(function()
{
	GameEvents.Subscribe( "healthBarsUpdate", UpdateHealthBars );
})();