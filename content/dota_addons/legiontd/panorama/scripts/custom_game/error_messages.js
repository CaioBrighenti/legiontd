"use strict";
var messageStartTime

function ShowErrorMessage( data )
{
	var error_type = data.errorType
	$( "#ErrorMessagesLabel" ).text = $.Localize( "#" + error_type)
	$.GetContextPanel().SetHasClass( "error_message_visible", true );
	messageStartTime = Game.GetGameTime();
	Game.EmitSound("General.CommandRestricted");
}

function HideErrorMessage( )
{
	$.GetContextPanel().SetHasClass( "error_message_visible", false );
}

function UpdateTimer()
{
	var gameTime = Game.GetGameTime();
	if (gameTime - 1 >= messageStartTime) {
		HideErrorMessage();
	};
		
	$.Schedule( 0.1, UpdateTimer );
}


(function()
{
	GameEvents.Subscribe( "error_message_show", ShowErrorMessage );
	UpdateTimer();
})();
