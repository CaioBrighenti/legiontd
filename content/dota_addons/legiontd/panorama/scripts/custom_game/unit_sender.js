"use strict";
var ToggleButonPressed = false
var AccordionToggled = 1

function OnToggleSender()
{
	if (ToggleButonPressed) {
		ToggleButonPressed = false;
		$.GetContextPanel().SetHasClass( "toggle_unit_sender", false );
		Game.EmitSound( "ui_team_select_lock_and_start" );
	} else {
		ToggleButonPressed = true;
		$.GetContextPanel().SetHasClass( "toggle_unit_sender", true );
		Game.EmitSound( "ui_team_select_lock_and_start" );
		if (AccordionToggled == -1) {
			$.GetContextPanel().SetHasClass( "toggle_accordion_1", true );
			AccordionToggled = 1
		};
	};
}

function OnAccordionPress1()
{
	if (AccordionToggled == 1) {
		AccordionToggled = 0;
		$.GetContextPanel().SetHasClass( "toggle_accordion_1", false );
		Game.EmitSound( "ui_team_select_lock_and_start" );
	} else {
		AccordionToggled = 1;
		$.GetContextPanel().SetHasClass( "toggle_accordion_1", true );
		$.GetContextPanel().SetHasClass( "toggle_accordion_2", false );
		$.GetContextPanel().SetHasClass( "toggle_accordion_3", false );
		Game.EmitSound( "ui_team_select_lock_and_start" );
	};
}

function OnAccordionPress2()
{
	if (AccordionToggled == 2) {
		AccordionToggled = 0;
		$.GetContextPanel().SetHasClass( "toggle_accordion_2", false );
		Game.EmitSound( "ui_team_select_lock_and_start" );
	} else {
		AccordionToggled = 2;
		$.GetContextPanel().SetHasClass( "toggle_accordion_2", true );
		$.GetContextPanel().SetHasClass( "toggle_accordion_1", false );
		$.GetContextPanel().SetHasClass( "toggle_accordion_3", false );
		Game.EmitSound( "ui_team_select_lock_and_start" );
	};
}

function OnAccordionPress3()
{
	if (AccordionToggled == 3) {
		AccordionToggled = 0;
		$.GetContextPanel().SetHasClass( "toggle_accordion_3", false );
		Game.EmitSound( "ui_team_select_lock_and_start" );
	} else {
		AccordionToggled = 3;
		$.GetContextPanel().SetHasClass( "toggle_accordion_3", true );
		$.GetContextPanel().SetHasClass( "toggle_accordion_1", false );
		$.GetContextPanel().SetHasClass( "toggle_accordion_2", false );
		Game.EmitSound( "ui_team_select_lock_and_start" );
	};
}

function Accordion1Button1Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion1Button1ShowTooltip()
{
	var abilityButton = $( "#Accordion1Button1" );
	var unitName = $.Localize( "#Accordion1Button1Name" );
	var abilityInfo = $.Localize( "#Accordion1Button1Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion1Button1HideTooltip()
{
	var abilityButton = $( "#Accordion1Button1" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion1Button2Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion1Button2ShowTooltip()
{
	var abilityButton = $( "#Accordion1Button2" );
	var unitName = $.Localize( "#Accordion1Button2Name" );
	var abilityInfo = $.Localize( "#Accordion1Button2Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion1Button2HideTooltip()
{
	var abilityButton = $( "#Accordion1Button2" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion1Button3Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion1Button3ShowTooltip()
{
	var abilityButton = $( "#Accordion1Button3" );
	var unitName = $.Localize( "#Accordion1Button3Name" );
	var abilityInfo = $.Localize( "#Accordion1Button3Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion1Button3HideTooltip()
{
	var abilityButton = $( "#Accordion1Button3" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion1Button4Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion1Button4ShowTooltip()
{
	var abilityButton = $( "#Accordion1Button4" );
	var unitName = $.Localize( "#Accordion1Button4Name" );
	var abilityInfo = $.Localize( "#Accordion1Button4Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion1Button4HideTooltip()
{
	var abilityButton = $( "#Accordion1Button4" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion1Button5Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion1Button5ShowTooltip()
{
	var abilityButton = $( "#Accordion1Button5" );
	var unitName = $.Localize( "#Accordion1Button5Name" );
	var abilityInfo = $.Localize( "#Accordion1Button5Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion1Button5HideTooltip()
{
	var abilityButton = $( "#Accordion1Button5" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion1Button6Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion1Button6ShowTooltip()
{
	var abilityButton = $( "#Accordion1Button6" );
	var unitName = $.Localize( "#Accordion1Button6Name" );
	var abilityInfo = $.Localize( "#Accordion1Button6Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion1Button6HideTooltip()
{
	var abilityButton = $( "#Accordion1Button6" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion1Button7Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion1Button7ShowTooltip()
{
	var abilityButton = $( "#Accordion1Button7" );
	var unitName = $.Localize( "#Accordion1Button7Name" );
	var abilityInfo = $.Localize( "#Accordion1Button7Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion1Button7HideTooltip()
{
	var abilityButton = $( "#Accordion1Button7" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion1Button8Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion1Button8ShowTooltip()
{
	var abilityButton = $( "#Accordion1Button8" );
	var unitName = $.Localize( "#Accordion1Button8Name" );
	var abilityInfo = $.Localize( "#Accordion1Button8Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion1Button8HideTooltip()
{
	var abilityButton = $( "#Accordion1Button8" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion2Button1Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion2Button1ShowTooltip()
{
	var abilityButton = $( "#Accordion2Button1" );
	var unitName = $.Localize( "#Accordion2Button1Name" );
	var abilityInfo = $.Localize( "#Accordion2Button1Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion2Button1HideTooltip()
{
	var abilityButton = $( "#Accordion2Button1" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion2Button2Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion2Button2ShowTooltip()
{
	var abilityButton = $( "#Accordion2Button2" );
	var unitName = $.Localize( "#Accordion2Button2Name" );
	var abilityInfo = $.Localize( "#Accordion2Button2Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion2Button2HideTooltip()
{
	var abilityButton = $( "#Accordion2Button2" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion2Button3Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion2Button3ShowTooltip()
{
	var abilityButton = $( "#Accordion2Button3" );
	var unitName = $.Localize( "#Accordion2Button3Name" );
	var abilityInfo = $.Localize( "#Accordion2Button3Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion2Button3HideTooltip()
{
	var abilityButton = $( "#Accordion2Button3" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion2Button4Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion2Button4ShowTooltip()
{
	var abilityButton = $( "#Accordion2Button4" );
	var unitName = $.Localize( "#Accordion2Button4Name" );
	var abilityInfo = $.Localize( "#Accordion2Button4Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion2Button4HideTooltip()
{
	var abilityButton = $( "#Accordion2Button4" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion2Button5Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion2Button5ShowTooltip()
{
	var abilityButton = $( "#Accordion2Button5" );
	var unitName = $.Localize( "#Accordion2Button5Name" );
	var abilityInfo = $.Localize( "#Accordion2Button5Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion2Button5HideTooltip()
{
	var abilityButton = $( "#Accordion2Button5" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion2Button6Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion2Button6ShowTooltip()
{
	var abilityButton = $( "#Accordion2Button6" );
	var unitName = $.Localize( "#Accordion2Button6Name" );
	var abilityInfo = $.Localize( "#Accordion2Button6Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion2Button6HideTooltip()
{
	var abilityButton = $( "#Accordion2Button6" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion2Button7Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion2Button7ShowTooltip()
{
	var abilityButton = $( "#Accordion2Button7" );
	var unitName = $.Localize( "#Accordion2Button7Name" );
	var abilityInfo = $.Localize( "#Accordion2Button7Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion2Button7HideTooltip()
{
	var abilityButton = $( "#Accordion2Button7" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion2Button8Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion2Button8ShowTooltip()
{
	var abilityButton = $( "#Accordion2Button8" );
	var unitName = $.Localize( "#Accordion2Button8Name" );
	var abilityInfo = $.Localize( "#Accordion2Button8Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion2Button8HideTooltip()
{
	var abilityButton = $( "#Accordion1Button8" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion3Button1Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion3Button1ShowTooltip()
{
	var abilityButton = $( "#Accordion3Button1" );
	var unitName = $.Localize( "#Accordion3Button1Name" );
	var abilityInfo = $.Localize( "#Accordion3Button1Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion3Button1HideTooltip()
{
	var abilityButton = $( "#Accordion3Button1" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion3Button2Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion3Button2ShowTooltip()
{
	var abilityButton = $( "#Accordion3Button2" );
	var unitName = $.Localize( "#Accordion3Button2Name" );
	var abilityInfo = $.Localize( "#Accordion3Button2Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion3Button2HideTooltip()
{
	var abilityButton = $( "#Accordion3Button2" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion3Button3Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion3Button3ShowTooltip()
{
	var abilityButton = $( "#Accordion3Button3" );
	var unitName = $.Localize( "#Accordion3Button3Name" );
	var abilityInfo = $.Localize( "#Accordion3Button3Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion3Button3HideTooltip()
{
	var abilityButton = $( "#Accordion3Button3" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion3Button4Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion3Button4ShowTooltip()
{
	var abilityButton = $( "#Accordion3Button4" );
	var unitName = $.Localize( "#Accordion3Button4Name" );
	var abilityInfo = $.Localize( "#Accordion3Button4Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion3Button4HideTooltip()
{
	var abilityButton = $( "#Accordion3Button4" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion3Button5Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion3Button5ShowTooltip()
{
	var abilityButton = $( "#Accordion3Button5" );
	var unitName = $.Localize( "#Accordion3Button5Name" );
	var abilityInfo = $.Localize( "#Accordion3Button5Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion3Button5HideTooltip()
{
	var abilityButton = $( "#Accordion3Button5" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion3Button6Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion3Button6ShowTooltip()
{
	var abilityButton = $( "#Accordion3Button6" );
	var unitName = $.Localize( "#Accordion3Button6Name" );
	var abilityInfo = $.Localize( "#Accordion3Button6Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion3Button6HideTooltip()
{
	var abilityButton = $( "#Accordion3Button6" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion3Button7Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion3Button7ShowTooltip()
{
	var abilityButton = $( "#Accordion3Button7" );
	var unitName = $.Localize( "#Accordion3Button7Name" );
	var abilityInfo = $.Localize( "#Accordion3Button7Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion3Button7HideTooltip()
{
	var abilityButton = $( "#Accordion3Button7" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function Accordion3Button8Press()
{
	Game.EmitSound( "General.Buy" );
}
function Accordion3Button8ShowTooltip()
{
	var abilityButton = $( "#Accordion3Button8" );
	var unitName = $.Localize( "#Accordion3Button8Name" );
	var abilityInfo = $.Localize( "#Accordion3Button8Info" );
	$.DispatchEvent( "DOTAShowTitleTextTooltip", abilityButton, unitName, abilityInfo );
}
function Accordion3Button8HideTooltip()
{
	var abilityButton = $( "#Accordion1Button8" );
	$.DispatchEvent( "DOTAHideTitleTextTooltip", abilityButton );
}

function UpdateCanBuild(data)
{
	if (data.canBuild == true) {
		$.GetContextPanel().SetHasClass( "hide_building", false	 );
	} else {
		$.GetContextPanel().SetHasClass( "hide_building", true );
	};
}


(function()
{
	$.GetContextPanel().SetHasClass( "toggle_accordion_1", true	 );
	GameEvents.Subscribe( "canBuild_Update", UpdateCanBuild );
})();
