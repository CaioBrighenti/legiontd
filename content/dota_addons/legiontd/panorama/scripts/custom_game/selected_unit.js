"use strict";

function UpdateSelectedUnit()
{
	var selectedUnitPanel = $( "#SelectedUnitPanel" );
	if ( !selectedUnitPanel )
		return;
	
	var queryUnit = Players.GetLocalPlayerPortraitUnit();
	$( "#UnitName" ).text = $.Localize(Entities.GetUnitName(queryUnit));
	$( "#HealthBarLabel" ).text = Entities.GetHealth(queryUnit);

	if (Entities.IsRealHero(queryUnit)) {
		//$( "#UpgradeButton" ).visible = false
		//$( "#RemoveButton" ).visible = false
		$.GetContextPanel().SetHasClass( "toggle_control_buttons", false );
	} else {
		$( "#UpgradeButton" ).visible = true
		$( "#RemoveButton" ).visible = true
		$.GetContextPanel().SetHasClass( "toggle_control_buttons", true );
	};
}

function OnRemoveButton() {
	var localPlayer = Players.GetLocalPlayer();
	var selectedUnit = Players.GetLocalPlayerPortraitUnit(); 
	GameEvents.SendCustomGameEventToServer("legiontd_unit_remove", {player: localPlayer, unit: selectedUnit })
}

function OnUpgradeButton() {
	var localPlayer = Players.GetLocalPlayer();
	var selectedUnit = Players.GetLocalPlayerPortraitUnit(); 
	GameEvents.SendCustomGameEventToServer("legiontd_unit_upgrade", {player: localPlayer, unit: selectedUnit })	
}

(function()
{
	GameEvents.Subscribe( "dota_player_update_selected_unit", UpdateSelectedUnit );
	GameEvents.Subscribe( "dota_player_update_query_unit", UpdateSelectedUnit );
	
})();
