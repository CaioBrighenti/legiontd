"use strict";

function UpdateTimer()
{

    if (GameUI.GetClickBehaviors() == 3) {
    	var mousePosWorld = GameUI.GetScreenWorldPosition(GameUI.GetCursorPosition());
    	if (mousePosWorld) {
    		mousePosWorld[0] = Game.WorldToScreenX(mousePosWorld[0], mousePosWorld[1], mousePosWorld[2]) - 960
    		mousePosWorld[1] = Game.WorldToScreenY(mousePosWorld[0], mousePosWorld[1], mousePosWorld[2]) - 535
    		$.GetContextPanel().style.position = mousePosWorld[0] + "px " + mousePosWorld[1] + "px " + "0px";
    		$.GetContextPanel().style.transform = "rotateX( 30deg )"
    		$.GetContextPanel().visible = true
    	};
    } else {
    	$.GetContextPanel().visible = false
    };
    $.Schedule( 0.1, UpdateTimer );
}

(function()
{
	UpdateTimer();
})();


