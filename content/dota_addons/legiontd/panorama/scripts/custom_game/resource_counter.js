"use strict";

function UpdateMoneyCounter() 
{
	var playerID = Players.GetLocalPlayer()
	$( "#GoldCounterLabel" ).text = Players.GetGold( playerID )
};

(function()
{
	GameEvents.Subscribe( "dota_money_changed", UpdateMoneyCounter );
})();